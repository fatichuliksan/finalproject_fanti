<?php

namespace models;

class MasterJenisBelanja extends Model
{
    protected $table = "MASTER_JENIS_BELANJA";
    protected $primary_key = "ID_MASTER_JENIS_BELANJA";
    protected $deleted_at = true;
}
