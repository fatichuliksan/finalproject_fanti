<?php

namespace models;

class MasterSatuan extends Model
{
    protected $table = "MASTER_SATUAN";
    protected $primary_key = "ID_MASTER_SATUAN";
    protected $deleted_at = true;
}
