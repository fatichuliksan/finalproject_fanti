<?php

namespace models;

class PenggunaRole extends Model
{
    protected $table = "PENGGUNA_ROLE";
    protected $primary_key = "ID_PENGGUNA_ROLE";
    protected $deleted_at = false;
} 
