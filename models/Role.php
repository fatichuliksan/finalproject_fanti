<?php

namespace models;

class Role extends Model
{
    protected $table = "ROLE";
    protected $primary_key = "ID_ROLE";
}
