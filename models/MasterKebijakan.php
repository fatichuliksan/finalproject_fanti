<?php

namespace models;

class MasterKebijakan extends Model
{
    protected $table = "MASTER_KEBIJAKAN";
    protected $primary_key = "ID_MASTER_KEBIJAKAN";
    protected $deleted_at = true;
}
