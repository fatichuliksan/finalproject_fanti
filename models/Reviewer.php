<?php

namespace models;

class Reviewer extends Model
{
    protected $table = "REVIEWER";
    protected $primary_key = "ID_REVIEWER";
    protected $deleted_at = true;

    public function getAll(){
        return $this->query("select id_reviewer, reviewer.tahun,reviewer.reviewer as reviewer_ke, pengguna.nama, uk1.label  label_uk1, uk2.label label_uk2
        from reviewer
        join pengguna on pengguna.id_pengguna=reviewer.id_pengguna
        join master_unit_kerja uk1 on uk1.id_master_unit_kerja=pengguna.id_master_unit_kerja
        join master_unit_kerja uk2 on uk2.id_master_unit_kerja=reviewer.id_master_unit_kerja
        where reviewer.deleted_at is null
        order by reviewer.created_at desc, reviewer.updated_at desc
        ");
    }
}