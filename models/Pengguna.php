<?php

namespace models;

use configs\DB;

class Pengguna extends Model
{
    protected $table = "PENGGUNA";
    protected $primary_key = "ID_PENGGUNA";
    protected $deleted_at = true;

    public function getAll()
    {
        return $this->query("select * from pengguna
        join master_unit_kerja on master_unit_kerja.id_master_unit_kerja=pengguna.id_master_unit_kerja
        where pengguna.deleted_at is null
        order by nama asc");
    }
}
