<?php

namespace models;

class MasterSbm extends Model
{
    protected $table = "MASTER_SBM";
    protected $primary_key = "ID_MASTER_SBM";
    protected $deleted_at = true;
}
