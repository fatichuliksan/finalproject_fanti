<?php

namespace models;

class Menu extends Model
{
    protected $table = "MENU";
    protected $primary_key = "ID_MENU";
    protected $deleted_at = false;
}
