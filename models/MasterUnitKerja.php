<?php

namespace models;

class MasterUnitKerja extends Model
{
    protected $table = "MASTER_UNIT_KERJA";
    protected $primary_key = "ID_MASTER_UNIT_KERJA";
    protected $deleted_at = true;
}
