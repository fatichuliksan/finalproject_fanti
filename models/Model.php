<?php

namespace models;

use configs\DB;
use configs\Helper;
use PDO;

class Model
{
    private $db;
    private $selectQuery = " select  * ";
    private $tableQuery = " from ";
    private $whereQuery = " where 1=1 ";
    private $orderQuery = "";
    private $query = "";

    public function __construct()
    {
        $this->db = DB::getInstance();
        $this->tableQuery .= isset($this->table) ? $this->table : "";
    }

    public function find($id)
    {
        return $this->db->getFirst("select * from " . $this->table . " where " . (($this->primary_key) ? $this->primary_key : 'ID') . "=" . $id);
    }

    public function all()
    {
        $deleted_at = false;
        if (isset($this->deleted_at)) {
            $deleted_at = $this->deleted_at;
            if ($deleted_at)
                $this->whereQuery .= " and deleted_at is null";
        }

        $result = $this->db->getAll($this->selectQuery  . $this->tableQuery . $this->whereQuery);
        return ($result) ? $result : [];
    }

    public function first()
    {
        $deleted_at = false;
        if (isset($this->deleted_at)) {
            $deleted_at = $this->deleted_at;
            if ($deleted_at)
                $this->whereQuery .= " and deleted_at is null";
        }
        $result = $this->db->getFirst($this->selectQuery  . $this->tableQuery . $this->whereQuery);
        return ($result) ? $result : [];
    }


    public function where($key, $opt = " = ", $value)
    {
        $this->whereQuery .= " and " . $key .  $opt . (is_string($value) ? "'" . $value . "'" : $value);
        return $this;
    }

    public function whereRaw($params)
    {
        $this->whereQuery .= " and " . $params;
        return $this;
    }

    public function get()
    {
        $deleted_at = false;
        if (isset($this->deleted_at)) {
            $deleted_at = $this->deleted_at;
            if ($deleted_at)
                $this->whereQuery .= " and deleted_at is null";
        }
        $result = $this->db->selectRaw($this->selectQuery  . $this->tableQuery . $this->whereQuery . $this->orderQuery);
        return ($result) ? $result : [];
    }

    public function create($array)
    {
        return $this->db->insert($this->table, $array);
    }

    public function createGetId($array)
    {
        return $this->db->insertGetId($this->table, $array, $this->primary_key ?: "id");
    }

    public function update($array)
    {
        if ($this->whereQuery == " where 1=1 ") {
            $this->whereQuery = " where 1=2 ";
        }
        return $this->db->update($this->table, $array, $this->whereQuery);
    }

    public function delete($id = null)
    {
        $deleted_at = false;
        if (isset($this->deleted_at)) {
            $deleted_at = $this->deleted_at;
        }

        if ($id)
            $this->whereQuery .= " and " . $this->primary_key . "=" . $id;

        if ($deleted_at) {
            return $this->db->updateRaw("UPDATE " . $this->table . " SET DELETED_AT=SYSDATE " . $this->whereQuery);
        } else {
            return $this->db->delete($this->table, $this->whereQuery);
        }
    }

    public function select($array)
    {
        return $this->db->insertGetId($this->table, $array, $this->primary_key ?: "id");
    }

    public function orderBy($field, $type = 'asc')
    {
        if ($this->orderQuery == "") {
            $this->orderQuery .= " order by " . $field . " " . $type;
        } else {
            $this->orderQuery .= ', ' . $field . " " . $type;
        }
        return $this;
    }

    public function query($q)
    {
        return $this->db->getAll($q);
    }
}
