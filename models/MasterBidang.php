<?php

namespace models;

class MasterBidang extends Model
{
    protected $table = "MASTER_BIDANG";
    protected $primary_key = "ID_MASTER_BIDANG";
    protected $deleted_at = true;
}
