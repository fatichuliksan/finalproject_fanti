<?php

namespace models;

class MasterPagu extends Model
{
    protected $table = "MASTER_PAGU";
    protected $primary_key = "ID_MASTER_PAGU";
    protected $deleted_at = true;
}
