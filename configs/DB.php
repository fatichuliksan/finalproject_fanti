<?php

namespace configs;

use Exception;
use PDO;
use PDOException;

class DB extends PDO
{
    private $db_type = CONFIG["database"]["default"]["db"] . ":";
    private $host = (CONFIG["database"]["default"]["host"]);
    private $port = CONFIG["database"]["default"]["port"];
    private $username = CONFIG["database"]["default"]["username"];
    private $password = CONFIG["database"]["default"]["password"];
    private $db_name = "dbname=" . CONFIG["database"]["default"]["db_name"];

    private static $instance;

    public function __construct()
    {
        $host = $this->host ? "host=" . $this->host . ($this->port ? ":" . $this->port : "") . ";" : "";
        parent::__construct(
            $this->db_type . $host . $this->db_name,
            $this->username,
            $this->password,
            [
                PDO::ATTR_PERSISTENT => true,
                // PDO::ATTR_AUTOCOMMIT => false,
            ]
        );
    }

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public function getAll($query)
    {
        $pre = $this->prepare($query);
        $pre->execute();
        if ($this->errorInfo()[2]) {
            throw new Exception($this->errorInfo()[2], $this->errorInfo()[1]);
            return false;
        } else
            return $pre->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getFirst($query)
    {
        $pre = $this->prepare($query);
        $pre->execute();
        if ($this->errorInfo()[2]) {
            throw new Exception($this->errorInfo()[2], $this->errorInfo()[1]);
            return false;
        } else
            return $pre->fetch(PDO::FETCH_ASSOC);
    }

    private function getKeyToString($values)
    {
        $field = "";
        $jml = count($values);
        $i = 0;
        foreach ($values as $key => $item) {
            if ($i < $jml - 1) {
                $field .= $key . ",";
            } else {
                $field .= $key;
            }
            $i++;
        }
        return $field;
    }

    private function getKeyToStringBind($values)
    {
        $value = "";
        $jml = count($values);
        $i = 0;
        foreach ($values as $key => $item) {
            if ($i < $jml - 1) {
                $value .= ":" . $key . ",";
            } else {
                $value .= ":" . $key;
            }
            $i++;
        }
        return $value;
    }

    public function bindByName($pre, $param)
    {
        if ($param) {
            foreach ($param as $key => $value) {
                $pre->bindParam(':' . $key,  $param[$key]);
            }
        }
    }
    public function insert($table, $values = [])
    {
        $field = $this->getKeyToString($values);
        $value = $this->getKeyToStringBind($values);
        $pre = $this->prepare("INSERT INTO " . $table . " (" . $field . ") VALUES (" . $value . ")");
        $this->bindByName($pre, $values);
        $pre->execute();
        if ($this->errorInfo()[2]) {
            throw new Exception($this->errorInfo()[2], $this->errorInfo()[1]);
            return false;
        } else
            return true;
    }

    public function insertGetId($table, $values = [], $attr)
    {
        $field = $this->getKeyToString($values);
        $value = $this->getKeyToStringBind($values);
        $pre = $this->prepare("INSERT INTO " . $table . " (" . $field . ") VALUES (" . $value . ") returning " . $attr . " into :" . $attr);
        $this->bindByName($pre, $values);
        $pre->bindParam($attr, $id, PDO::PARAM_INT, 9);
        $pre->execute();
        if ($this->errorInfo()[2]) {
            throw new Exception($this->errorInfo()[2], $this->errorInfo()[1]);
            return false;
        } else
            return $id;
    }



    public function update($table,  $values = [], $where)
    {
        try {
            $set = '';
            $i = 0;
            $jml = count($values);
            foreach ($values as $key => $item) {
                if ($i < $jml - 1) {
                    $set .=  $key . "='" . $item . "'" . ",";
                } else {
                    $set .=  $key . "='" . $item . "'";
                }
                $i++;
            }

            $q = "UPDATE " . $table . " SET " . $set . $where;
            echo $q;
            $pre = $this->prepare($q);
            $pre->execute();
            if ($this->errorInfo()[2]) {
                throw new Exception($this->errorInfo()[2], $this->errorInfo()[1]);
                return false;
            } else
                return true;
        } catch (PDOException $e) {
            die($e);
        }
    }

    public function delete($table, $condition)
    {
        $pre = $this->prepare("Delete from " . $table  . $condition);
        $pre->execute();
        if ($this->errorInfo()[2]) {
            throw new Exception($this->errorInfo()[2], $this->errorInfo()[1]);
            return false;
        } else
            return true;
    }

    public function selectRaw($query)
    {
        return $this->getAll($query);
    }

    public function updateRaw($query)
    {
        $pre = $this->prepare($query);
        $pre->execute();
        if ($this->errorInfo()[2]) {
            throw new Exception($this->errorInfo()[2], $this->errorInfo()[1]);
            return false;
        } else
            return true;
    }
    public function insertRaw($query)
    {
        $pre = $this->prepare($query);
        $pre->execute();
        if ($this->errorInfo()[2]) {
            throw new Exception($this->errorInfo()[2], $this->errorInfo()[1]);
            return false;
        } else
            return true;
    }

    public function deleteRaw($query)
    {
        $pre = $this->prepare($query);
        $pre->execute();
        if ($this->errorInfo()[2]) {
            throw new Exception($this->errorInfo()[2], $this->errorInfo()[1]);
            return false;
        } else
            return true;
    }

    protected $transactionCounter = 0;

    public function beginTransaction()
    {
        if (!$this->transactionCounter++) {
            return parent::beginTransaction();
        }
        $this->exec('SAVEPOINT trans' . $this->transactionCounter);
        return $this->transactionCounter >= 0;
    }

    public function commit()
    {
        if (!--$this->transactionCounter) {
            return parent::commit();
        }
        return $this->transactionCounter >= 0;
    }

    public function rollback()
    {
        if (--$this->transactionCounter) {
            $this->exec('ROLLBACK TO trans' . $this->transactionCounter + 1);
            return true;
        }
        return parent::rollback();
    }
}
