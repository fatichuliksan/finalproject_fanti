<?php
spl_autoload_extensions(".php");
spl_autoload_register();
$requestUri = str_replace(HTTP_FOLDER, "/", $_SERVER['REQUEST_URI']);

$test = explode("/", $requestUri);
$jml = count($test);
if ($jml % 2 == 1) {
    if (isset($test[$jml - 1])) {
        if ($test[$jml - 1] == '')
            unset($test[$jml - 1]);
    }
    $url = implode("/", $test);
} else {
    $url = $requestUri;
}
if (strpos($url, "?")) {
    $url = substr($url, 0, strpos($url, "?"));
}
$method = $_SERVER["REQUEST_METHOD"];
