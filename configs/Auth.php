<?php

namespace configs;

use configs\Helper;

class Auth
{
    public $isLoged = '';
    public function __construct()
    {
        if (isset($_SESSION['user'])) {
            $this->isLoged =  true;
        } else {
            $this->isLoged =  false;
        }
    }
    public function check()
    {
        return $this->isLoged;
    }

    public function middleware()
    {
        if ($this->isLoged) {
            return true;
        } else {
            Helper::redirect("login");
        }
    }
}
