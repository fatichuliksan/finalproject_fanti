<?php

namespace configs;

class Helper
{
    public function getBaseDir()
    {
        return str_replace("\configs", "", __DIR__);
    }

    public static function redirect($url = "home")
    {
        HEADER("location:" . BASE_URL . $url);
        die();
    }

    public static function redirectBack()
    {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        die();
    }

    public static function dump($val = null)
    {
        header("Content-type:application/json");
        print_r(json_encode($val, true));
        die();
    }
}
