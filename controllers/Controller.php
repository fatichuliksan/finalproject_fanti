<?php

namespace controllers;

class Controller
{
    public $basePath = '';
    public $request = [];
    private $viewsPath = "/views";
    public function __construct()
    {
        $this->basePath = str_replace("\controllers", "", __DIR__);
        $this->request = $_REQUEST;
    }
    public function render($path, array $args)
    {
        extract(array_merge($args, ["base_path" => $this->basePath . $this->viewsPath]));
        ob_start();
        include($this->basePath . $this->viewsPath . $path);
        $var = ob_get_contents();
        ob_end_clean();

        if (isset($_SESSION['notifikasi'])) {
            unset($_SESSION['notifikasi']);
        }
        echo $var;
    }
}
