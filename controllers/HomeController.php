<?php

namespace controllers;

use controllers\Controller;
use models\MasterBidang;

class HomeController extends Controller
{
    public function index()
    {
        parent::render("/home/index.php", []);
    }
}
