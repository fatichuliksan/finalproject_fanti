<?php

namespace controllers;

use controllers\Controller;
use configs\Helper;
use models\Pengguna;

class LoginController extends Controller
{
    public function getLogin()
    {
        parent::render("/login/index.php", ["test" => "halo"]);
    }

    public function postLogin()
    {
        $email = $this->request['email'];
        $password = $this->request['password'];
        $result = (new Pengguna())->where("email", "=", $email)->first();
        if ($result) {
            if ($result["PASSWORD"] == $password) {
                $_SESSION['user'] = [
                    "id_pengguna" => $result["ID_PENGGUNA"],
                    "email" => $result["EMAIL"],
                    "nama" => $result["NAMA"],
                ];
            } else {
                $_SESSION['notifikasi'] = [
                    "type" => "danger",
                    "message" => "Password salah!"
                ];
            }
        } else {
            $_SESSION['notifikasi'] = [
                "type" => "danger",
                "message" => "Pengguna tidak ditemukan!"
            ];
        }
        Helper::redirect();
    }

    public function postLogout()
    {
        session_destroy();
        Helper::redirect();
    }
}
