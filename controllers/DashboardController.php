<?php

namespace controllers;

use controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        parent::render("/dashboard/index.php", []);
    }
}
