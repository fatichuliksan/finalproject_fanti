<?php

namespace controllers\master;

use configs\DB;
use configs\Helper;
use controllers\Controller;
use Exception;
use models\MasterBidang;
use models\MasterUnitKerja;
use models\Pengguna;
use models\PenggunaRole;
use models\Role;

class PenggunaController extends Controller
{
    private $view = "/master/pengguna/";
    private $url = "master/pengguna";
    public function index()
    {

        parent::render($this->view . "index.php", [
            "url" => $this->url,
            "data" => (new DB)->selectRaw("
            select id_pengguna, nama, email,unit_kerja, CREATED_AT, UPDATED_AT,
            LISTAGG(label, ', ') WITHIN GROUP (ORDER BY label) AS role
            from (
                select p.id_pengguna, nama, email, uk.label as unit_kerja, r.label, p.created_at, p.updated_at
                from pengguna p
                left join master_unit_kerja uk on uk.id_master_unit_kerja=p.id_master_unit_kerja
                left join PENGGUNA_ROLE pr on pr.id_pengguna=p.id_pengguna
                left join role r on r.id_role=pr.id_role
                where p.DELETED_AT is null
            )
            group by id_pengguna, nama, email,unit_kerja, CREATED_AT, UPDATED_AT
            order by CREATED_AT desc, UPDATED_AT DESC")
        ]);
    }

    public function create()
    {
        $pengguna = null;
        if (isset($_REQUEST['id'])) {
            $pengguna = (new Pengguna())->find($_REQUEST['id']);
            $pRole = (new PenggunaRole)->where("id_pengguna", "=", $pengguna["ID_PENGGUNA"])->get();

            $pengguna["ID_ROLES"] = array_column($pRole, 'ID_ROLE');
        }
        $param =  [
            "url" => $this->url,
            "data" => $pengguna,
            "listUnitKerja" => (new MasterUnitKerja())->all(),
            "listRole" => (new Role)->all()
        ];
        parent::render($this->view . "form.php", $param);
    }

    public function save()
    {
        $id_pengguna = $_POST['id_pengguna'];
        $nama =  $_POST['nama'];
        $email =  $_POST['email'];
        $password =  $_POST['password'];
        $id_unit_kerja =  $_POST['id_unit_kerja'];
        $id_roles =  $_POST['id_roles'];

        $db = new DB();
        $db->beginTransaction();
        try {
            $data = [
                "nama" => $nama,
                "email" => $email,
                "password" => $password,
                "id_master_unit_kerja" => $id_unit_kerja
            ];

            if ($id_pengguna) {
                $cek = (new Pengguna)->where("email", "=", $email)->where("id_pengguna", "!=", $id_pengguna)->get();
                if ($cek) {
                    $_SESSION['notifikasi'] = [
                        "type" => "danger",
                        "message" => "Email telah terdaftar!"
                    ];
                    Helper::redirectBack();
                }
                (new Pengguna)->where("id_pengguna", "=", $id_pengguna)->update($data);
                if ($id_pengguna) {
                    (new PenggunaRole)->where("id_pengguna", "=", $id_pengguna)->delete();
                    foreach ($id_roles as $val) {
                        (new PenggunaRole)->create([
                            "id_pengguna" => $id_pengguna,
                            "id_role" => $val
                        ]);
                    }
                }
            } else {
                $cek = (new Pengguna)->where("email", "=", $email)->get();
                if ($cek) {
                    $_SESSION['notifikasi'] = [
                        "type" => "danger",
                        "message" => "Email telah terdaftar!"
                    ];
                    Helper::redirectBack();
                }
                $id_pengguna = (new Pengguna)->createGetId($data);
                if ($id_pengguna) {
                    (new PenggunaRole)->where("id_pengguna", "=", $id_pengguna)->delete();
                    foreach ($id_roles as $val) {
                        (new PenggunaRole)->create([
                            "id_pengguna" => $id_pengguna,
                            "id_role" => $val
                        ]);
                    }
                }
            }
            $_SESSION['notifikasi'] = [
                "type" => "success",
                "message" => "Berhasil disimpan!"
            ];
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            $_SESSION['notifikasi'] = [
                "type" => "danger",
                "message" => "Gagal disimpan!"
            ];
        }
        Helper::redirect($this->url);
    }

    public function delete()
    {
        try {
            $result = (new Pengguna())->delete($_REQUEST['id']);
            $data = [
                "status_code" => 200,
                "message" => $result
            ];
            Helper::dump($data);
        } catch (Exception $e) {
            $data = [
                "status_code" => 500,
                "message" => $e->getMessage()
            ];
            Helper::dump($data);
        }
    }
}
