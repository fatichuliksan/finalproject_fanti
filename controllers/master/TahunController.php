<?php

namespace controllers\master;

use configs\Helper;
use controllers\Controller;
use Exception;
use models\Tahun;

class TahunController extends Controller
{
    private $view = "/master/tahun/";
    private $url = "master/tahun";
    public function index()
    {
        $data = new Tahun();
        parent::render($this->view . "index.php", [
            "url" => $this->url,
            "data" => $data->orderBy('tahun', 'desc')->get()
        ]);
    }

    public function create()
    {
        $data = null;
        if (isset($_REQUEST['id'])) {
            $data = (new Tahun())->find($_REQUEST['id']);
        }
        $param =  [
            "url" => $this->url,
            "data" => $data
        ];
        parent::render($this->view . "form.php", $param);
    }

    public function save()
    {
        $is_edit = $_POST['is_edit'];
        $tahun = $_POST['tahun'];
        $is_active = $_POST['is_active'];
        try {
            if ($is_edit == "true") {
                if ($is_active == 0) {
                    $cek = (new Tahun)->where("tahun", "!=", $tahun)
                        ->where("is_active", "=", 1)->get();
                    if (count($cek) == 0) {
                        $_SESSION['notifikasi'] = [
                            "type" => "danger",
                            "message" => "Salah satu tahun harus aktif!"
                        ];
                        Helper::redirectBack();
                    }
                }

                (new Tahun)->where("tahun", "=", $tahun)
                    ->update(["is_active" => $is_active]);

                if ($is_active == 1) {
                    (new Tahun)->where("tahun", "!=", $tahun)
                        ->update(["is_active" => 0]);
                }
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil diperbarui!"
                ];
            } else {
                $cek = (new Tahun)->find($tahun);
                if ($cek) {
                    $_SESSION['notifikasi'] = [
                        "type" => "danger",
                        "message" => "Tahun sudah tersedia!"
                    ];
                    Helper::redirectBack();
                }

                (new Tahun)->create([
                    "tahun" => $tahun,
                    "is_active" => $is_active
                ]);

                if ($is_active == 1) {
                    (new Tahun)->where("tahun", "!=", $tahun)
                        ->update(["is_active" => 0]);
                }

                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil disimpan!"
                ];
            }
        } catch (Exception $e) {
            $_SESSION['notifikasi'] = [
                "type" => "danger",
                "message" => "Gagal disimpan!"
            ];
        }
        Helper::redirect($this->url);
    }

    public function delete()
    {
        try {
            $result = (new MasterBidang())->delete($_REQUEST['id']);
            $data = [
                "status_code" => 200,
                "message" => $result
            ];
            Helper::dump($data);
        } catch (Exception $e) {
            $data = [
                "status_code" => 500,
                "message" => $e->getMessage()
            ];
            Helper::dump($data);
        }
    }
}
