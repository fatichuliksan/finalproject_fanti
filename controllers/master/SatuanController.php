<?php

namespace controllers\master;

use configs\Helper;
use controllers\Controller;
use Exception;
use models\MasterBidang;
use models\MasterSatuan;

class SatuanController extends Controller
{
    private $view = "/master/satuan/";
    private $url = "master/satuan";
    public function index()
    {
        parent::render($this->view . "index.php", [
            "url" => $this->url,
            "data" => (new MasterSatuan)->all()
        ]);
    }

    public function create()
    {
        $data = null;
        if (isset($_REQUEST['id'])) {
            $data = (new MasterSatuan())->find($_REQUEST['id']);
        }
        $param =  [
            "url" => $this->url,
            "data" => $data
        ];
        parent::render($this->view . "form.php", $param);
    }

    public function save()
    {
        $id_master_satuan = $_REQUEST['id_master_satuan'];
        $label =  $_REQUEST['label'];
        try {
            $data = ["LABEL" => $label];
            if ($id_master_satuan) {
                (new MasterSatuan)->where("id_master_satuan", "=", $id_master_satuan)
                    ->update($data);
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil diperbarui!"
                ];
            } else {
                (new MasterSatuan)->create($data);

                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil disimpan!"
                ];
            }
        } catch (Exception $e) {
            $_SESSION['notifikasi'] = [
                "type" => "danger",
                "message" => "Gagal disimpan!"
            ];
        }
        Helper::redirect($this->url);
    }

    public function delete()
    {
        try {
            $result = (new MasterSatuan())->delete($_REQUEST['id']);
            $data = [
                "status_code" => 200,
                "message" => $result
            ];
            Helper::dump($data);
        } catch (Exception $e) {
            $data = [
                "status_code" => 500,
                "message" => $e->getMessage()
            ];
            Helper::dump($data);
        }
    }
}
