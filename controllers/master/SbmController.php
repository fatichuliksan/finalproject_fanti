<?php

namespace controllers\master;

use configs\DB;
use configs\Helper;
use controllers\Controller;
use Exception;
use models\MasterJenisBelanja;
use models\MasterSatuan;
use models\MasterSbm;

class SbmController extends Controller
{
    private $view = "/master/sbm/";
    private $url = "master/sbm";
    public function index()
    {
        $db = new DB();
        $data = $db->selectRaw("
        select id_master_sbm, master_jenis_belanja.label as label_jb, master_satuan.label as label_s, harga, master_sbm.label,master_sbm.kode 
        from master_sbm 
        join master_jenis_belanja on master_jenis_belanja.id_master_jenis_belanja=master_sbm.id_master_jenis_belanja
        join master_satuan on master_satuan.id_master_satuan=master_sbm.id_master_satuan
        where master_sbm.deleted_at is null
        order by master_sbm.created_at desc,master_sbm.updated_at desc ");
        parent::render($this->view . "index.php", [
            "url" => $this->url,
            "data" => $data
        ]);
    }

    public function create()
    {

        $jenisBelanja = (new MasterJenisBelanja)->all();
        $satuan = (new MasterSatuan)->all();
        $data = null;
        if (isset($_REQUEST['id'])) {
            $data = (new MasterSbm())->find($_REQUEST['id']);
        }
        $param =  [
            "url" => $this->url,
            "data" => $data,
            "jenisBelanja" => $jenisBelanja,
            "satuan" => $satuan,
        ];
        parent::render($this->view . "form.php", $param);
    }

    public function save()
    {
        $id_master_sbm = $_POST['id_master_sbm'];
        try {
            $data = [
                "ID_MASTER_JENIS_BELANJA" =>  $_POST['id_master_jenis_belanja'],
                "ID_MASTER_SATUAN" =>  $_POST['id_master_satuan'],
                "KODE" =>  $_POST['kode'],
                "LABEL" =>  $_POST['label'],
                "HARGA" =>  $_POST['harga'],
            ];
            if ($id_master_sbm) {
                (new MasterSbm)->where("id_master_sbm", "=", $id_master_sbm)
                    ->update($data);
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil diperbarui!"
                ];
            } else {
                (new MasterSbm)->create($data);
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil disimpan!"
                ];
            }
        } catch (Exception $e) {
            $_SESSION['notifikasi'] = [
                "type" => "danger",
                "message" => "Gagal disimpan!"
            ];
        }
        Helper::redirect($this->url);
    }

    public function delete()
    {
        try {
            $result = (new MasterSbm())->delete($_REQUEST['id']);
            $data = [
                "status_code" => 200,
                "message" => $result
            ];
            Helper::dump($data);
        } catch (Exception $e) {
            $data = [
                "status_code" => 500,
                "message" => $e->getMessage()
            ];
            Helper::dump($data);
        }
    }
}
