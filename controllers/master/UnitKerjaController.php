<?php

namespace controllers\master;

use configs\DB;
use configs\Helper;
use controllers\Controller;
use Exception;
use models\MasterBidang;
use models\MasterUnitKerja;

class UnitKerjaController extends Controller
{
    private $view = "/master/unit_kerja/";
    private $url = "master/unit-kerja";
    public function index()
    {
        $db = new DB();
        $masterUnitKerja = $db->getAll("
        select id_master_unit_kerja, master_unit_kerja.label as label_uk, master_bidang.label as label_b
        from master_unit_kerja 
        join master_bidang on master_bidang.id_master_bidang=master_unit_kerja.id_master_bidang
        where master_unit_kerja.deleted_at is null");
        parent::render($this->view . "index.php", [
            "url" => $this->url,
            "masterUnitKerja" => $masterUnitKerja
        ]);
    }

    public function create()
    {

        $bidang = (new MasterBidang)->all();
        $unitKerja = null;
        if (isset($_REQUEST['id'])) {
            $unitKerja = (new MasterUnitKerja())->find($_REQUEST['id']);
        }
        $param =  [
            "url" => $this->url,
            "unitKerja" => $unitKerja,
            "bidang" => $bidang,
        ];
        parent::render($this->view . "form.php", $param);
    }

    public function save()
    {
        $id_master_unit_kerja = $_POST['id_master_unit_kerja'];
        try {
            if ($id_master_unit_kerja) {
                (new MasterUnitKerja)->where("ID_MASTER_UNIT_KERJA", "=", $id_master_unit_kerja)
                    ->update(["LABEL" =>  $_POST['label'], "ID_MASTER_BIDANG" => $_POST["id_master_bidang"]]);
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil diperbarui!"
                ];
            } else {
                // Helper::dump($_POST);
                (new MasterUnitKerja)->create([
                    "ID_MASTER_BIDANG" => $_POST["id_master_bidang"],
                    "LABEL" =>  $_POST['label'],
                ]);

                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil disimpan!"
                ];
            }
        } catch (Exception $e) {
            $_SESSION['notifikasi'] = [
                "type" => "danger",
                "message" => "Gagal disimpan!"
            ];
        }
        Helper::redirect($this->url);
    }

    public function delete()
    {
        try {
            $result = (new MasterUnitKerja())->delete($_REQUEST['id']);
            $data = [
                "status_code" => 200,
                "message" => $result
            ];
            Helper::dump($data);
        } catch (Exception $e) {
            $data = [
                "status_code" => 500,
                "message" => $e->getMessage()
            ];
            Helper::dump($data);
        }
    }
}
