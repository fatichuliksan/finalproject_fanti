<?php

namespace controllers\master;

use configs\Helper;
use controllers\Controller;
use Exception;
use models\MasterJenisBelanja;

class JenisBelanjaController extends Controller
{
    private $view = "/master/jenis_belanja/";
    private $url = "master/jenis-belanja";
    public function index()
    {
        parent::render($this->view . "index.php", [
            "url" => $this->url,
            "data" => (new MasterJenisBelanja)->all()
        ]);
    }

    public function create()
    {
        $data = null;
        if (isset($_REQUEST['id'])) {
            $data = (new MasterJenisBelanja())->find($_REQUEST['id']);
        }
        $param =  [
            "url" => $this->url,
            "data" => $data
        ];
        parent::render($this->view . "form.php", $param);
    }

    public function save()
    {
        $id_master_jenis_belanja = $_POST['id_master_jenis_belanja'];
        try {
            $data = [
                "KODE" => $_POST['kode'],
                "LABEL" => $_POST['label'],
                "KETERANGAN" => $_POST['keterangan'],
            ];
            if ($id_master_jenis_belanja) {
                (new MasterJenisBelanja)->where("id_master_jenis_belanja", "=", $id_master_jenis_belanja)
                    ->update($data);
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil diperbarui!"
                ];
            } else {
                (new MasterJenisBelanja)->create($data);

                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil disimpan!"
                ];
            }
        } catch (Exception $e) {
            $_SESSION['notifikasi'] = [
                "type" => "danger",
                "message" => "Gagal disimpan!"
            ];
        }
        Helper::redirect($this->url);
    }

    public function delete()
    {
        try {
            $result = (new MasterJenisBelanja())->delete($_REQUEST['id']);
            $data = [
                "status_code" => 200,
                "message" => $result
            ];
            Helper::dump($data);
        } catch (Exception $e) {
            $data = [
                "status_code" => 500,
                "message" => $e->getMessage()
            ];
            Helper::dump($data);
        }
    }
}
