<?php

namespace controllers\master;

use configs\Helper;
use controllers\Controller;
use Exception;
use models\MasterKebijakan;

class KebijakanController extends Controller
{
    private $view = "/master/kebijakan/";
    private $url = "master/kebijakan";
    public function index()
    {
        parent::render($this->view . "index.php", [
            "url" => $this->url,
            "data" => (new MasterKebijakan)->all()
        ]);
    }

    public function create()
    {
        $data = null;
        if (isset($_REQUEST['id'])) {
            $data = (new MasterKebijakan())->find($_REQUEST['id']);
        }
        $param =  [
            "url" => $this->url,
            "data" => $data
        ];
        parent::render($this->view . "form.php", $param);
    }

    public function save()
    {
        $id_master_kebijakan = $_POST['id_master_kebijakan'];
        try {
            $data = [
                "LABEL" => $_POST['label'],
                "URL" => $_POST['url'],
            ];
            if ($id_master_kebijakan) {
                (new MasterKebijakan)->where("id_master_kebijakan", "=", $id_master_kebijakan)
                    ->update($data);
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil diperbarui!"
                ];
            } else {
                (new MasterKebijakan)->create($data);

                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil disimpan!"
                ];
            }
        } catch (Exception $e) {
            $_SESSION['notifikasi'] = [
                "type" => "danger",
                "message" => "Gagal disimpan!"
            ];
        }
        Helper::redirect($this->url);
    }

    public function delete()
    {
        try {
            $result = (new MasterKebijakan())->delete($_REQUEST['id']);
            $data = [
                "status_code" => 200,
                "message" => $result
            ];
            Helper::dump($data);
        } catch (Exception $e) {
            $data = [
                "status_code" => 500,
                "message" => $e->getMessage()
            ];
            Helper::dump($data);
        }
    }
}
