<?php

namespace controllers\master;

use configs\DB;
use configs\Helper;
use controllers\Controller;
use Exception;
use models\MasterBidang;
use models\MasterPagu;
use models\MasterUnitKerja;
use models\Tahun;

class PaguController extends Controller
{
    private $view = "/master/pagu/";
    private $url = "master/pagu";
    public function index()
    {
        $db = new DB();
        $data = $db->getAll("
        select * from master_pagu
        join master_bidang on master_bidang.id_master_bidang=master_pagu.id_master_bidang
        where master_pagu.deleted_at is null
        order by master_pagu.created_at desc, master_pagu.updated_at desc
        ");
        parent::render($this->view . "index.php", [
            "url" => $this->url,
            "data" => $data
        ]);
    }

    public function create()
    {
        $bidang = (new MasterBidang)->all();
        $data = null;
        $tahun =  (new Tahun)->orderBy('tahun', 'desc')->get();
        if (isset($_REQUEST['id'])) {
            $data = (new MasterPagu())->find($_REQUEST['id']);
        }
        $param =  [
            "url" => $this->url,
            "data" => $data,
            "bidang" => $bidang,
            "tahun" => $tahun
        ];
        parent::render($this->view . "form.php", $param);
    }

    public function save()
    {
        $id_master_pagu = $_POST['id_master_pagu'];
        try {
            $data = [
                "tahun" =>  $_POST['tahun'],
                "total" =>  $_POST['total'],
                "ID_MASTER_BIDANG" => $_POST["id_master_bidang"]
            ];
            if ($id_master_pagu) {
                (new MasterPagu)->where("id_master_pagu", "=", $id_master_pagu)
                    ->update($data);
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil diperbarui!"
                ];
            } else {
                (new MasterPagu)->create($data);
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil disimpan!"
                ];
            }
        } catch (Exception $e) {
            $_SESSION['notifikasi'] = [
                "type" => "danger",
                "message" => "Gagal disimpan!"
            ];
        }
        Helper::redirect($this->url);
    }

    public function delete()
    {
        try {
            $result = (new MasterPagu())->delete($_REQUEST['id']);
            $data = [
                "status_code" => 200,
                "message" => $result
            ];
            Helper::dump($data);
        } catch (Exception $e) {
            $data = [
                "status_code" => 500,
                "message" => $e->getMessage()
            ];
            Helper::dump($data);
        }
    }
}
