<?php

namespace controllers\master;

use configs\Helper;
use controllers\Controller;
use Exception;
use models\MasterUnitKerja;
use models\Pengguna;
use models\Reviewer;
use models\Tahun;

class ReviewerController extends Controller
{
    private $view = "/master/reviewer/";
    private $url = "master/reviewer";
    public function index()
    {
        parent::render($this->view . "index.php", [
            "url" => $this->url,
            "data" => (new Reviewer)->getAll()
        ]);
    }

    public function create()
    {
        $data = null;
        if (isset($_REQUEST['id'])) {
            $data = (new Reviewer())->find($_REQUEST['id']);
        }
        $param =  [
            "url" => $this->url,
            "data" => $data,
            "listUnitKerja" => (new MasterUnitKerja())->all(),
            "listPengguna" => (new Pengguna)->getAll(),
            "listTahun" => (new Tahun)->orderBy('tahun', 'desc')->get(),
        ];
        parent::render($this->view . "form.php", $param);
    }

    public function save()
    {
        $id_reviewer = $_POST['ID_REVIEWER'];
        try {
            $data = [
                "ID_PENGGUNA" =>  $_POST['ID_PENGGUNA'],
                "ID_MASTER_UNIT_KERJA" =>  $_POST['ID_MASTER_UNIT_KERJA'],
                "REVIEWER" =>  $_POST['REVIEWER'],
                "TAHUN" =>  $_POST['TAHUN'],
            ];
            if ($id_reviewer) {
                (new Reviewer)->where("ID_REVIEWER", "=", $id_reviewer)
                    ->update($data);
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil diperbarui!"
                ];
            } else {
                (new Reviewer)->create($data);
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil disimpan!"
                ];
            }
        } catch (Exception $e) {
            $_SESSION['notifikasi'] = [
                "type" => "danger",
                "message" => "Gagal disimpan!"
            ];
        }
        Helper::redirect($this->url);
    }

    public function delete()
    {
        try {
            $result = (new Reviewer())->delete($_REQUEST['id']);
            $data = [
                "status_code" => 200,
                "message" => $result
            ];
            Helper::dump($data);
        } catch (Exception $e) {
            $data = [
                "status_code" => 500,
                "message" => $e->getMessage()
            ];
            Helper::dump($data);
        }
    }
}
