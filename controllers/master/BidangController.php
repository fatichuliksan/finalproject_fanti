<?php

namespace controllers\master;

use configs\Helper;
use controllers\Controller;
use Exception;
use models\MasterBidang;

class BidangController extends Controller
{
    private $view = "/master/bidang/";
    private $url = "master/bidang";
    public function index()
    {
        $masterBidang = new MasterBidang();
        parent::render($this->view . "index.php", [
            "url" => $this->url,
            "masterBidang" => $masterBidang->all()
        ]);
    }

    public function create()
    {
        $bidang = null;
        if (isset($_REQUEST['id'])) {
            $bidang = (new MasterBidang())->find($_REQUEST['id']);
        }
        $param =  [
            "url" => $this->url,
            "bidang" => $bidang
        ];
        parent::render($this->view . "form.php", $param);
    }

    public function save()
    {
        $id_master_bidang = $_REQUEST['id_master_bidang'];
        $label =  $_REQUEST['label'];
        try {
            if ($id_master_bidang) {
                (new MasterBidang)->where("ID_MASTER_BIDANG", "=", $id_master_bidang)
                    ->update(["LABEL" => $label]);
                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil diperbarui!"
                ];
            } else {
                (new MasterBidang)->create([
                    "LABEL" => $label
                ]);

                $_SESSION['notifikasi'] = [
                    "type" => "success",
                    "message" => "Berhasil disimpan!"
                ];
            }
        } catch (Exception $e) {
            $_SESSION['notifikasi'] = [
                "type" => "danger",
                "message" => "Gagal disimpan!"
            ];
        }
        Helper::redirect($this->url);
    }

    public function delete()
    {
        try {
            $result = (new MasterBidang())->delete($_REQUEST['id']);
            $data = [
                "status_code" => 200,
                "message" => $result
            ];
            Helper::dump($data);
        } catch (Exception $e) {
            $data = [
                "status_code" => 500,
                "message" => $e->getMessage()
            ];
            Helper::dump($data);
        }
    }
}
