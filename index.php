<?php

use configs\Helper;
use models\Menu;

session_start();
// error_reporting(E_ALL);
error_reporting(-1);
DEFINE('BASE_PATH', __DIR__);
DEFINE('BASE_PATH_VIEW', __DIR__ . "/views");
DEFINE('BASE_PATH_CONFIG', __DIR__ . "/configs");
// DEFINE('HTTP_TYPE', $_SERVER['HTTP_X_FORWARDED_PROTO']);
DEFINE('HTTP_ROOT', $_SERVER['HTTP_HOST']);
DEFINE('HTTP_FOLDER', dirname($_SERVER['PHP_SELF']) . '/');
DEFINE("CONFIG", json_decode(file_get_contents("config.json"), true));
$document_root = str_replace("\\", "/", substr(__DIR__, strlen($_SERVER['DOCUMENT_ROOT'])));
// DEFINE('BASE_URL', HTTP_TYPE . "://" . HTTP_ROOT . substr(__DIR__, strlen($_SERVER['DOCUMENT_ROOT'])) . '/');
DEFINE('BASE_URL', ((CONFIG["url"])? CONFIG["url"] : "http://" . HTTP_ROOT) . $document_root . '/');
require_once BASE_PATH . "/configs/autoloader.php";

include('route.php');
