<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pagu Anggaran</title>

    <?php include(BASE_PATH_VIEW . "/layout/css.php") ?>
</head>

<body id="page-top">
    <?php include(BASE_PATH_VIEW . "/layout/navbar.php") ?>
    <div id="wrapper">
        <?php include(BASE_PATH_VIEW . "/layout/sidebar.php") ?>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Master
                    </li>
                    <li class="breadcrumb-item active">
                        Pagu Anggaran
                    </li>
                </ol>
            </div>
            <div class="row">
                <div class="container my-auto">
                    <div class="card mb-3">
                        <div class="card-header">
                            <?php echo (($data) ? 'Edit' : 'Tambah') ?> Pagu Anggaran
                        </div>
                        <div class="card-body">
                            <form action="<?php echo BASE_URL . $url . "/save" ?>" method="POST">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Bidang</label>
                                    <div class="col-sm-10">
                                        <select name="id_master_bidang" id="" required>
                                            <option value="">pilih</option>
                                            <?php foreach ($bidang as $b) { ?>
                                                <option value="<?php echo $b["ID_MASTER_BIDANG"] ?>" <?php echo (($b["ID_MASTER_BIDANG"] == $data['ID_MASTER_BIDANG']) ? "selected" : "") ?>><?php echo $b["LABEL"] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tahun</label>
                                    <div class="col-sm-10">
                                        <select name="tahun" id="">
                                            <?php foreach ($tahun as $t) { ?>
                                                <option value="<?php echo $t["TAHUN"] ?>" <?php echo (($t["TAHUN"] == $data['TAHUN']) ? "selected" : "") ?>><?php echo $t["TAHUN"] ?> <?php echo ($t["IS_ACTIVE"]==1)?'(AKTIF)':'' ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Total (Rp)</label>
                                    <div class="col-sm-10">
                                        <input type="number" min="0" class="form-control" name="total" value="<?php echo ($data) ? $data['TOTAL'] : '' ?>" placeholder="Total Pagu Anggaran">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-10">
                                        <input type="hidden" name="id_master_pagu" value="<?php echo ($data) ? $data['ID_MASTER_PAGU'] : '' ?>">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <button type="button" class="btn btn-danger" onclick=" window.history.back()">Kembali</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <?php include(BASE_PATH_VIEW . "/layout/footer.php") ?>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <?php include(BASE_PATH_VIEW . "/layout/scroll_top.php") ?>
    <?php include(BASE_PATH_VIEW . "/layout/js.php") ?>
    <script>
        $(document).ready(function() {
            $("select").select2({
                width: "100%"
            });
        });
    </script>
</body>

</html>