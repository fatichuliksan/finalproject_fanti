<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pengguna</title>

    <?php include(BASE_PATH_VIEW . "/layout/css.php") ?>
</head>

<body id="page-top">
    <?php include(BASE_PATH_VIEW . "/layout/navbar.php") ?>
    <div id="wrapper">
        <?php include(BASE_PATH_VIEW . "/layout/sidebar.php") ?>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Master
                    </li>
                    <li class="breadcrumb-item active">
                        Pengguna
                    </li>
                </ol>
            </div>
            <div class="row">
                <div class="container my-auto">
                    <div class="card mb-3">
                        <div class="card-header">
                            <?php echo (($data) ? 'Edit' : 'Tambah') ?> Pengguna
                        </div>
                        <div class="card-body">
                            <?php include(BASE_PATH_VIEW . '/layout/alert.php') ?>
                            <form action="<?php echo BASE_URL . $url . "/save" ?>" method="POST">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama</label>
                                    <div class="col-sm-10">
                                        <input type="text" required class="form-control" name="nama" value="<?php echo ($data) ? $data['NAMA'] : '' ?>" placeholder="Nama">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" required class="form-control" name="email" value="<?php echo ($data) ? $data['EMAIL'] : '' ?>" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" required class="form-control" name="password" value="<?php echo ($data) ? $data['PASSWORD'] : '' ?>" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Unit Kerja</label>
                                    <div class="col-sm-10">
                                        <select name="id_unit_kerja" id="" class="form-control" required>
                                            <option value="">pilih</option>
                                            <?php foreach ($listUnitKerja as $i) { ?>
                                                <option value="<?php echo $i["ID_MASTER_UNIT_KERJA"] ?>" <?php echo (($i["ID_MASTER_UNIT_KERJA"] == $data['ID_MASTER_UNIT_KERJA']) ? "selected" : "") ?>><?php echo $i["LABEL"] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Role</label>
                                    <div class="col-sm-10">
                                        <select name="id_roles[]" id="" class="form-control" multiple>
                                            <?php foreach ($listRole as $i) { ?>
                                                <option value="<?php echo $i["ID_ROLE"] ?>" <?php echo ((in_array($i["ID_ROLE"], ($data["ID_ROLES"]?$data["ID_ROLES"]:[]))) ? "selected" : "") ?>><?php echo $i["LABEL"] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-10">
                                        <input type="hidden" name="id_pengguna" value="<?php echo ($data) ? $data['ID_PENGGUNA'] : '' ?>">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <button type="button" class="btn btn-danger" onclick=" window.history.back()">Kembali</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <?php include(BASE_PATH_VIEW . "/layout/footer.php") ?>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <?php include(BASE_PATH_VIEW . "/layout/scroll_top.php") ?>
    <?php include(BASE_PATH_VIEW . "/layout/js.php") ?>
    <script>
        $(document).ready(function() {
            $("select").select2({
                width: "100%"
            });
        });
    </script>
</body>

</html>