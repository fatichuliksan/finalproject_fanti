<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Reviewer</title>

    <?php include(BASE_PATH_VIEW . "/layout/css.php") ?>
</head>

<body id="page-top">
    <?php include(BASE_PATH_VIEW . "/layout/navbar.php") ?>
    <div id="wrapper">
        <?php include(BASE_PATH_VIEW . "/layout/sidebar.php") ?>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Master
                    </li>
                    <li class="breadcrumb-item active">
                        Reviewer
                    </li>
                </ol>
            </div>
            <div class="row">
                <div class="container my-auto">
                    <div class="card mb-3">
                        <div class="card-header">
                            <?php echo (($data) ? 'Edit' : 'Tambah') ?> Reviewer
                        </div>
                        <div class="card-body">
                            <form action="<?php echo BASE_URL . $url . "/save" ?>" method="POST">
                            <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tahun</label>
                                    <div class="col-sm-10">
                                        <select name="TAHUN" id="">
                                            <?php foreach ($listTahun as $t) { ?>
                                                <option value="<?php echo $t["TAHUN"] ?>" <?php echo (($t["TAHUN"] == $data['TAHUN']) ? "selected" : "") ?>><?php echo $t["TAHUN"] ?> <?php echo ($t["IS_ACTIVE"]==1)?'(AKTIF)':'' ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Pengguna</label>
                                    <div class="col-sm-10">
                                        <select name="ID_PENGGUNA" id="" required>
                                            <option value="">pilih</option>
                                            <?php foreach ($listPengguna as $i) { ?>
                                                <option value="<?php echo $i["ID_PENGGUNA"] ?>" <?php echo (($i["ID_PENGGUNA"] == $data['ID_PENGGUNA']) ? "selected" : "") ?>><?php echo $i["NAMA"] ?> (<?php echo $i["LABEL"] ?>)</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Unit Kerja (yg akan di-review)</label>
                                    <div class="col-sm-10">
                                        <select name="ID_MASTER_UNIT_KERJA" id="" required>
                                            <option value="">pilih</option>
                                            <?php foreach ($listUnitKerja as $i) { ?>
                                                <option value="<?php echo $i["ID_MASTER_UNIT_KERJA"] ?>" <?php echo (($i["ID_MASTER_UNIT_KERJA"] == $data['ID_MASTER_UNIT_KERJA']) ? "selected" : "") ?>><?php echo $i["LABEL"] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Reviewer Ke</label>
                                    <div class="col-sm-10">
                                        <select name="REVIEWER" id="" required>
                                            <option value="1" <?php echo (($data['REVIEWER']==1) ? "selected" : "") ?>>1</option>
                                            <option value="2" <?php echo (($data['REVIEWER']==2) ? "selected" : "") ?>>2</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-10">
                                        <input type="hidden" name="ID_REVIEWER" value="<?php echo ($data) ? $data['ID_REVIEWER'] : '' ?>">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <button type="button" class="btn btn-danger" onclick=" window.history.back()">Kembali</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <?php include(BASE_PATH_VIEW . "/layout/footer.php") ?>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <?php include(BASE_PATH_VIEW . "/layout/scroll_top.php") ?>
    <?php include(BASE_PATH_VIEW . "/layout/js.php") ?>
    <script>
        $(document).ready(function() {
            $("select").select2({
                width: "100%"
            });
        });
    </script>
</body>

</html>