<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tambah Jenis Belanja</title>

    <?php include(BASE_PATH_VIEW . "/layout/css.php") ?>
</head>

<body id="page-top">
    <?php include(BASE_PATH_VIEW . "/layout/navbar.php") ?>
    <div id="wrapper">
        <?php include(BASE_PATH_VIEW . "/layout/sidebar.php") ?>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Master
                    </li>
                    <li class="breadcrumb-item active">
                        Jenis Belanja
                    </li>
                </ol>
            </div>
            <div class="row">
                <div class="container my-auto">
                    <div class="card mb-3">
                        <div class="card-header">
                            <?php echo (($data) ? 'Edit' : 'Tambah') ?> Jenis Belanja
                        </div>
                        <div class="card-body">
                            <form action="<?php echo BASE_URL . $url . "/save" ?>" method="POST">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="kode" value="<?php echo ($data) ? $data['KODE'] : '' ?>" placeholder="Kode">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Label</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="label" value="<?php echo ($data) ? $data['LABEL'] : '' ?>" placeholder="Label">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Keterangan</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="keterangan" value="<?php echo ($data) ? $data['KETERANGAN'] : '' ?>" placeholder="Keterangan">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-10">
                                        <input type="hidden" name="id_master_jenis_belanja" value="<?php echo ($data) ? $data['ID_MASTER_JENIS_BELANJA'] : '' ?>">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <button type="button" class="btn btn-danger" onclick=" window.history.back()">Kembali</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <?php include(BASE_PATH_VIEW . "/layout/footer.php") ?>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <?php include(BASE_PATH_VIEW . "/layout/scroll_top.php") ?>
    <?php include(BASE_PATH_VIEW . "/layout/js.php") ?>
</body>

</html>