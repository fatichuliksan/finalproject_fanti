<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SBM</title>

    <?php include(BASE_PATH_VIEW . "/layout/css.php") ?>
</head>

<body id="page-top">
    <?php include(BASE_PATH_VIEW . "/layout/navbar.php") ?>
    <div id="wrapper">
        <?php include(BASE_PATH_VIEW . "/layout/sidebar.php") ?>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Master
                    </li>
                    <li class="breadcrumb-item active">
                        SBM
                    </li>
                </ol>
            </div>
            <div class="row">
                <div class="container my-auto">
                    <div class="card mb-3">
                        <div class="card-header">
                            <?php echo (($data) ? 'Edit' : 'Tambah') ?> SBM
                        </div>
                        <div class="card-body">
                            <form action="<?php echo BASE_URL . $url . "/save" ?>" method="POST">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Jenis Belanja</label>
                                    <div class="col-sm-10">
                                        <select name="id_master_jenis_belanja" id="" required>
                                            <option value="">pilih</option>
                                            <?php foreach ($jenisBelanja as $b) { ?>
                                                <option value="<?php echo $b["ID_MASTER_JENIS_BELANJA"] ?>" <?php echo (($b["ID_MASTER_JENIS_BELANJA"] == $data['ID_MASTER_JENIS_BELANJA']) ? "selected" : "") ?>><?php echo $b["LABEL"] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Satuan</label>
                                    <div class="col-sm-10">
                                        <select name="id_master_satuan" id="" required>
                                            <option value="">pilih</option>
                                            <?php foreach ($satuan as $b) { ?>
                                                <option value="<?php echo $b["ID_MASTER_SATUAN"] ?>" <?php echo (($b["ID_MASTER_SATUAN"] == $data['ID_MASTER_SATUAN']) ? "selected" : "") ?>><?php echo $b["LABEL"] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="kode" value="<?php echo ($data) ? $data['KODE'] : '' ?>" placeholder="Kode">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Label</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="label" value="<?php echo ($data) ? $data['LABEL'] : '' ?>" placeholder="Label">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Harga</label>
                                    <div class="col-sm-10">
                                        <input type="number" min="0" class="form-control" name="harga" value="<?php echo ($data) ? $data['HARGA'] : '' ?>" placeholder="Kode">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-10">
                                        <input type="hidden" name="id_master_sbm" value="<?php echo ($data) ? $data['ID_MASTER_SBM'] : '' ?>">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                        <button type="button" class="btn btn-danger" onclick=" window.history.back()">Kembali</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <?php include(BASE_PATH_VIEW . "/layout/footer.php") ?>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <?php include(BASE_PATH_VIEW . "/layout/scroll_top.php") ?>
    <?php include(BASE_PATH_VIEW . "/layout/js.php") ?>
    <script>
        $(document).ready(function() {
            $("select").select2({
                width: "100%"
            });
        });
    </script>
</body>

</html>