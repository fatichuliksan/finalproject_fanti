<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kebijakan</title>

    <?php include(BASE_PATH_VIEW . "/layout/css.php") ?>
</head>

<body id="page-top">
    <?php include(BASE_PATH_VIEW . "/layout/navbar.php") ?>
    <div id="wrapper">
        <?php include(BASE_PATH_VIEW . "/layout/sidebar.php") ?>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        Master
                    </li>
                    <li class="breadcrumb-item active">
                        Kebijakan
                    </li>
                </ol>
            </div>
            <div class="row">
                <div class="container my-auto">
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fas fa-table"></i>
                            Data Kebijakan
                            <a href="<?php echo BASE_URL . $url . "/create" ?>" class="btn btn-outline-primary btn-default float-right">Tambah</a>
                        </div>
                        <div class="card-body">
                            <?php include(BASE_PATH_VIEW . '/layout/alert.php') ?>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th width="5%">No.</th>
                                            <th>Label</th>
                                            <th>URL</th>
                                            <th width="5%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($data as $i => $value) { ?>
                                            <tr>
                                                <td><?php echo $i + 1 ?></td>
                                                <td><?php echo $value['LABEL'] ?></td>
                                                <td><a href="//<?php echo $value['URL'] ?>" target="_blank">url</a></td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button class="btn-sm btn btn-outline-dark dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Pilihan
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" href="<?php echo BASE_URL . $url . "/create?id=" . $value["ID_MASTER_KEBIJAKAN"] ?>">Edit</a>
                                                            <a class="dropdown-item" href="javascript:void(0)" onclick="hapus('<?php echo BASE_URL . $url . '/delete?id=' . $value['ID_MASTER_KEBIJAKAN'] ?>')">Hapus</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <?php include(BASE_PATH_VIEW . "/layout/footer.php") ?>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <?php include(BASE_PATH_VIEW . "/layout/scroll_top.php") ?>
    <?php include(BASE_PATH_VIEW . "/layout/js.php") ?>
</body>

</html>