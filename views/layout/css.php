  <!-- Custom fonts for this template-->
  <link href="<?php echo BASE_URL ?>public/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?php echo BASE_URL ?>public/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="<?php echo BASE_URL ?>public/vendor/sweetalert2/sweetalert2.min.css" rel="stylesheet">
  <link href="<?php echo BASE_URL ?>public/vendor/select2/css/select2.min.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo BASE_URL ?>public/css/sb-admin.css" rel="stylesheet">