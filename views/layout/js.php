 <!-- Bootstrap core JavaScript-->
 <script src="<?php echo BASE_URL ?>public/vendor/jquery/jquery.min.js"></script>
 <script src="<?php echo BASE_URL ?>public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

 <!-- Core plugin JavaScript-->
 <script src="<?php echo BASE_URL ?>public/vendor/jquery-easing/jquery.easing.min.js"></script>

 <!-- Page level plugin JavaScript-->
 <script src="<?php echo BASE_URL ?>public/vendor/chart.js/Chart.min.js"></script>
 <script src="<?php echo BASE_URL ?>public/vendor/datatables/jquery.dataTables.js"></script>
 <script src="<?php echo BASE_URL ?>public/vendor/datatables/dataTables.bootstrap4.js"></script>
 <script src="<?php echo BASE_URL ?>public/vendor/sweetalert2/sweetalert2.min.js"></script>
 <script src="<?php echo BASE_URL ?>public/vendor/select2/js/select2.full.min.js"></script>

 <!-- Custom scripts for all pages-->
 <script src="<?php echo BASE_URL ?>public/js/sb-admin.min.js"></script>

 <!-- Demo scripts for this page-->
 <script src="<?php echo BASE_URL ?>public/js/demo/datatables-demo.js"></script>
 <script src="<?php echo BASE_URL ?>public/js/demo/chart-area-demo.js"></script>

 <script>
     function hapus(url) {
         Swal.fire({
             title: 'anda yakin akan hapus data ini?',
             // text: "You won't be able to revert this!",
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Ya',
             cancelButtonText: 'Tidak'
         }).then((result) => {
             if (result.value) {
                 $.ajax({
                     url: url,
                     type: 'DELETE',
                     dataType: "json",
                     success: function(res) {
                         console.log(res);
                         if (res.status_code == 200) {
                             Swal.fire(
                                 'Berhasil!',
                                 'Data telah dihapus',
                                 'success'
                             )

                             setTimeout(function() {

                                 window.location.reload();
                             }, 2000)
                         } else {
                             Swal.fire(
                                 'Gagal!',
                                 'Data gagal dihapus',
                                 'error'
                             )
                         }
                     },
                     error: function() {
                         Swal.fire(
                             'Gagal!',
                             'Data gagal dihapus',
                             'error'
                         )
                     }
                 });
             }
         })
     }
 </script>