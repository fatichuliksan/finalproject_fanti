<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <?php

    use models\Menu;

    $menus = (new Menu())->whereRaw("parent_id=0")->get();
    foreach ($menus as $i => $menu) { ?>
        <?php
            $menuChild = (new Menu())->whereRaw("PARENT_ID=" . $menu["ID_MENU"])->get();
            if (count($menuChild) == 0) { ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php if ($menu['URL'] != '#')
                                                        echo BASE_URL . $menu['URL'];
                                                    else
                                                        echo BASE_URL . $menu['URL'];
                                                    ?>">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span><?php echo $menu["LABEL"] ?></span>
                </a>
            </li>
        <?php } else { ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-folder"></i>
                    <span><?php echo $menu["LABEL"] ?></span></a>
                <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                    <?php foreach ($menuChild as $i => $child) { ?>
                        <a class="dropdown-item" href="<?php if ($child['URL'] != '#')
                                                                        echo BASE_URL . $child['URL'];
                                                                    else
                                                                        echo BASE_URL . $child['URL'];
                                                                    ?>"><?php echo $child["LABEL"] ?></a>
                    <?php } ?>
                </div>
            <?php } ?>
            </li>
        <?php } ?>
</ul>