<?php

use configs\Auth;
use controllers\LoginController;
use controllers\DashboardController;
use controllers\HomeController;
use controllers\master\BidangController;
use controllers\master\JenisBelanjaController;
use controllers\master\KebijakanController;
use controllers\master\PaguController;
use controllers\master\PenggunaController;
use controllers\master\ReviewerController;
use controllers\master\SatuanController;
use controllers\master\SbmController;
use controllers\master\TahunController;
use controllers\master\UnitKerjaController;
use models\MasterKebijakan;

$auth = new Auth();
$login = new LoginController();
$home = new HomeController();
$dashboard = new DashboardController();

//master
$tahun = new TahunController();
$bidang = new BidangController();
$unitKerja = new UnitKerjaController();
$pengguna = new PenggunaController();
$pagu = new PaguController();
$satuan = new SatuanController();
$kebijakan = new KebijakanController;
$jenisBelanja = new JenisBelanjaController;
$sbm = new SbmController;
$reviewer = new ReviewerController;
switch (true) {
    case ($url == "/login" && $method == "GET"):
        $login->getLogin();
        break;
    case ($url == "/login" && $method == "POST"):
        $login->postLogin();
        break;
    case (($url == "/" || $url == "/home") && $method == "GET" && $auth->middleware()):
        $home->index();
        break;
    case ($url == "/dashboard" && $method == "GET" && $auth->middleware()):
        $dashboard->index();
        break;
    case ($url == "/logout" && $method == "POST" && $auth->middleware()):
        $login->postLogout();
        break;
    case ($url == "/master/tahun" && $method == "GET" && $auth->middleware()):
        $tahun->index();
        break;
    case ($url == "/master/tahun/create" && $method == "GET" && $auth->middleware()):
        $tahun->create();
        break;
    case ($url == "/master/tahun/save" && $method == "POST" && $auth->middleware()):
        $tahun->save();
        break;
    case ($url == "/master/tahun/delete" && $method == "DELETE" && $auth->middleware()):
        $tahun->delete();
        break;
    case ($url == "/master/bidang" && $method == "GET" && $auth->middleware()):
        $bidang->index();
        break;
    case ($url == "/master/bidang/create" && $method == "GET" && $auth->middleware()):
        $bidang->create();
        break;
    case ($url == "/master/bidang/save" && $method == "POST" && $auth->middleware()):
        $bidang->save();
        break;
    case ($url == "/master/bidang/delete" && $method == "DELETE" && $auth->middleware()):
        $bidang->delete();
        break;
    case ($url == "/master/unit-kerja" && $method == "GET" && $auth->middleware()):
        $unitKerja->index();
        break;
    case ($url == "/master/unit-kerja/create" && $method == "GET" && $auth->middleware()):
        $unitKerja->create();
        break;
    case ($url == "/master/unit-kerja/save" && $method == "POST" && $auth->middleware()):
        $unitKerja->save();
        break;
    case ($url == "/master/unit-kerja/delete" && $method == "DELETE" && $auth->middleware()):
        $unitKerja->delete();
        break;
    case ($url == "/master/pengguna" && $method == "GET" && $auth->middleware()):
        $pengguna->index();
        break;
    case ($url == "/master/pengguna/create" && $method == "GET" && $auth->middleware()):
        $pengguna->create();
        break;
    case ($url == "/master/pengguna/save" && $method == "POST" && $auth->middleware()):
        $pengguna->save();
        break;
    case ($url == "/master/pengguna/delete" && $method == "DELETE" && $auth->middleware()):
        $pengguna->delete();
    case ($url == "/master/pagu" && $method == "GET" && $auth->middleware()):
        $pagu->index();
        break;
    case ($url == "/master/pagu/create" && $method == "GET" && $auth->middleware()):
        $pagu->create();
        break;
    case ($url == "/master/pagu/save" && $method == "POST" && $auth->middleware()):
        $pagu->save();
        break;
    case ($url == "/master/pagu/delete" && $method == "DELETE" && $auth->middleware()):
        $pagu->delete();
    case ($url == "/master/satuan" && $method == "GET" && $auth->middleware()):
        $satuan->index();
        break;
    case ($url == "/master/satuan/create" && $method == "GET" && $auth->middleware()):
        $satuan->create();
        break;
    case ($url == "/master/satuan/save" && $method == "POST" && $auth->middleware()):
        $satuan->save();
        break;
    case ($url == "/master/satuan/delete" && $method == "DELETE" && $auth->middleware()):
        $satuan->delete();
    case ($url == "/master/kebijakan" && $method == "GET" && $auth->middleware()):
        $kebijakan->index();
        break;
    case ($url == "/master/kebijakan/create" && $method == "GET" && $auth->middleware()):
        $kebijakan->create();
        break;
    case ($url == "/master/kebijakan/save" && $method == "POST" && $auth->middleware()):
        $kebijakan->save();
        break;
    case ($url == "/master/kebijakan/delete" && $method == "DELETE" && $auth->middleware()):
        $kebijakan->delete();
    case ($url == "/master/jenis-belanja" && $method == "GET" && $auth->middleware()):
        $jenisBelanja->index();
        break;
    case ($url == "/master/jenis-belanja/create" && $method == "GET" && $auth->middleware()):
        $jenisBelanja->create();
        break;
    case ($url == "/master/jenis-belanja/save" && $method == "POST" && $auth->middleware()):
        $jenisBelanja->save();
        break;
    case ($url == "/master/jenis-belanja/delete" && $method == "DELETE" && $auth->middleware()):
        $jenisBelanja->delete();
    case ($url == "/master/sbm" && $method == "GET" && $auth->middleware()):
        $sbm->index();
        break;
    case ($url == "/master/sbm/create" && $method == "GET" && $auth->middleware()):
        $sbm->create();
        break;
    case ($url == "/master/sbm/save" && $method == "POST" && $auth->middleware()):
        $sbm->save();
        break;
    case ($url == "/master/sbm/delete" && $method == "DELETE" && $auth->middleware()):
        $sbm->delete();
    case ($url == "/master/reviewer" && $method == "GET" && $auth->middleware()):
        $reviewer->index();
        break;
    case ($url == "/master/reviewer/create" && $method == "GET" && $auth->middleware()):
        $reviewer->create();
        break;
    case ($url == "/master/reviewer/save" && $method == "POST" && $auth->middleware()):
        $reviewer->save();
        break;
    case ($url == "/master/reviewer/delete" && $method == "DELETE" && $auth->middleware()):
        $reviewer->delete();
}
